# encoding: UTF-8

# Cookbook Name:: gitlab_fluentd
# Recipe:: production
# License:: MIT
#
# Copyright 2018, GitLab Inc.

include_recipe 'gitlab_fluentd::default'

template File.join(node['gitlab_fluentd']['config_dir_modules'], 'rails.conf') do
  owner 'root'
  group 'root'
  mode '0644'
  notifies :restart, 'service[td-agent]', :delayed
end

template File.join(node['gitlab_fluentd']['config_dir_modules'], 'unstructured.conf') do
  owner 'root'
  group 'root'
  mode '0644'
  notifies :restart, 'service[td-agent]', :delayed
end

template File.join(node['gitlab_fluentd']['config_dir_modules'], 'application.conf') do
  owner 'root'
  group 'root'
  mode '0644'
  notifies :restart, 'service[td-agent]', :delayed
end

# legacy templates
file File.join(node['gitlab_fluentd']['config_dir_modules'], 'production.conf') do
  action :delete
  notifies :restart, 'service[td-agent]', :delayed
end

file File.join(node['gitlab_fluentd']['config_dir_modules'], 'api.conf') do
  action :delete
  notifies :restart, 'service[td-agent]', :delayed
end

file File.join(node['gitlab_fluentd']['config_dir_modules'], 'geo.conf') do
  action :delete
  notifies :restart, 'service[td-agent]', :delayed
end

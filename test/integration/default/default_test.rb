# encoding: utf-8
# frozen_string_literal: true

# InSpec tests for recipe gitlab_fluentd::default

control 'general-checks' do
  impact 1.0
  title 'General tests for gitlab_fluentd cookbook'
  desc '
    This control ensures that:
      * td-agent is running'

  describe service('td-agent') do
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
  end
end

describe 'gitlab_fluentd::postgres' do
  before do
    allow_any_instance_of(Chef::Recipe).to receive(:get_secrets)
      .with('fake_backend', 'fake_path', 'fake_key')
      .and_return('elastic_cloud_host' => '1.2.3.4',
                  'elastic_cloud_port' => '1234',
                  'elastic_cloud_user' => 'root',
                  'elastic_cloud_password' => 'toor',
                  'pubsub_key' => '{"foo": "bar"}')
    stub_command('/usr/sbin/td-agent-gem list|grep -q ^fluent-plugin-google-cloud').and_return(0)
  end
  context 'when google cloud monitoring is enabled' do
    let(:chef_run) do
      ChefSpec::ServerRunner.new(platform: 'ubuntu',
                                 version: '16.04') do |node|
        node.normal['gitlab_fluentd']['stackdriver_enable'] = true
        node.normal['gitlab-server'] = {
          'rsyslog_client' => {
            'secrets' => {
              'backend' => 'fake_backend',
              'path' => 'fake_path',
              'key' => 'fake_key',
            },
          },
        }
        node.normal['gitlab_fluentd']['wale_enabled'] = true
        node.normal['gitlab_fluentd']['repmgrd_enabled'] = false
      end.converge(described_recipe)
    end

    it 'renders the postgres config correctly' do
      expect(chef_run).to render_file('/etc/td-agent/conf.d/postgres.conf').with_content { |content|
        expect(content).to eq(IO.read('spec/fixtures/postgres.template'))
      }
    end
  end
end

# encoding: UTF-8

# Cookbook:: gitlab_fluentd
# Spec:: default
#
# Copyright:: 2017, GitLab B.V., MIT.

require 'spec_helper'

describe 'gitlab_fluentd::default' do
  before do
    allow_any_instance_of(Chef::Recipe).to receive(:get_secrets)
      .with('fake_backend', 'fake_path', 'fake_key')
      .and_return('elastic_cloud_host' => '1.2.3.4',
                  'elastic_cloud_port' => '1234',
                  'elastic_cloud_user' => 'root',
                  'elastic_cloud_password' => 'toor',
                  'pubsub_key' => '{"foo": "bar"}')
    stub_command('/usr/sbin/td-agent-gem list|grep -q ^fluent-plugin-google-cloud').and_return(0)
  end

  context 'when all attributes are default, on Ubuntu 16.04' do
    let(:chef_run) do
      ChefSpec::ServerRunner.new(platform: 'ubuntu',
                                 version: '16.04') do |node|
        node.normal['gitlab-server'] = {
          'rsyslog_client' => {
            'secrets' => {
              'backend' => 'fake_backend',
              'path' => 'fake_path',
              'key' => 'fake_key',
            },
          },
        }
      end.converge(described_recipe)
    end

    it 'converges succesfully' do
      expect { chef_run }.to_not raise_error
    end

    it 'creates the sessions file' do
      expect(chef_run).to render_file('/etc/td-agent/conf.d/sessions.conf').with_content { |content|
        expect(content).to include '@type null'
      }
    end

    it 'does not by default create the pubsub plugin' do
      expect(chef_run).to_not create_file('/etc/td-agent/plugin/out_cloud_pubsub.rb')
    end
  end

  context 'when pubsub is enabled on Ubuntu 16.04' do
    let(:chef_run) do
      ChefSpec::ServerRunner.new(platform: 'ubuntu',
                                 version: '16.04') do |node|
        node.normal['gitlab_fluentd']['pubsub_enable'] = true
        node.normal['gitlab-server'] = {
          'rsyslog_client' => {
            'secrets' => {
              'backend' => 'fake_backend',
              'path' => 'fake_path',
              'key' => 'fake_key',
            },
          },
        }
      end.converge(described_recipe)
    end

    it 'converges succesfully' do
      expect { chef_run }.to_not raise_error
    end

    it 'creates the pubsub plugin' do
      expect(chef_run).to create_cookbook_file('/etc/td-agent/plugin/out_cloud_pubsub.rb')
      expect(chef_run).to(render_file('/etc/td-agent/plugin/out_cloud_pubsub.rb').with_content do |c|
        expect(c).to include('Originally copied from https://github.com/yosssi/fluent-plugin-cloud-pubsub')
      end)
    end
  end

  context 'when pubsub is enabled with a secret key on Ubuntu 16.04' do
    let(:chef_run) do
      ChefSpec::ServerRunner.new(platform: 'ubuntu',
                                 version: '16.04') do |node|
        node.normal['gitlab_fluentd']['pubsub_enable'] = true
        node.normal['gitlab_fluentd']['pubsub_key'] = 'in chef vault'
        node.normal['gitlab-server'] = {
          'rsyslog_client' => {
            'secrets' => {
              'backend' => 'fake_backend',
              'path' => 'fake_path',
              'key' => 'fake_key',
            },
          },
        }
      end.converge(described_recipe)
    end

    it 'converges succesfully' do
      expect { chef_run }.to_not raise_error
    end

    it 'creates the pubsub key file' do
      expect(chef_run).to create_file('/etc/td-agent/pubsub-gcp-key.json').with(
        content: '{"foo": "bar"}'
      )
    end
  end

  context 'when stackdriver is enabled' do
    let(:chef_run) do
      ChefSpec::ServerRunner.new(platform: 'ubuntu',
                                 version: '16.04') do |node|
        node.normal['gitlab_fluentd']['stackdriver_enable'] = true
        node.normal['gitlab-server'] = {
          'rsyslog_client' => {
            'secrets' => {
              'backend' => 'fake_backend',
              'path' => 'fake_path',
              'key' => 'fake_key',
            },
          },
        }
      end.converge(described_recipe)
    end

    it 'converges succesfully' do
      expect { chef_run }.to_not raise_error
    end

    it 'creates the sessions file' do
      expect(chef_run).to render_file('/etc/td-agent/conf.d/sessions.conf').with_content { |content|
        expect(content).to include '@type google_cloud'
      }
    end

    context 'and fluent-plugin-google-cloud is not installed' do
      it 'installs the gem' do
        stub_command('/usr/sbin/td-agent-gem list|grep -q ^fluent-plugin-google-cloud').and_return(false)
        expect(chef_run).to run_execute('install plugin-google-cloud gem')
      end
    end

    context 'and fluent-plugin-google-cloud is already installed' do
      it 'does not reinstall the gem' do
        stub_command('/usr/sbin/td-agent-gem list|grep -q ^fluent-plugin-google-cloud').and_return(true)
        expect(chef_run).to_not run_execute('install plugin-google-cloud gem')
      end
    end
  end
end
